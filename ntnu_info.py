import jb_database as jbd
import logging

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class NTNU:
    NO_SUBMISSION, SUBMITTED_HONESTY, SUBMITTED_CODE, SUBMITTED_REPORT, SUBMITTED_MARKED = 0,1,2,3,4

    def __init__(self, db):
        self.db = db

    @staticmethod
    def all_in_set( id, d_set = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ]
 ):
        start = 0
        stop = len(id) -1
        
        id = id.lower()
        for i in range(start,stop+1):
            if id[i] not in d_set:
                return False
        return True

    def validateId( self, ids ):
        id = ids.lower()
        
        digits = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ]

        if (id[0] == 'a' ):
            return True
        if ( id[0] == 'f' ):
            return True
        elif ( len(id) == 9 ):
            if ( id[0] in ['4', '6', '8']):
                if (id[-1] == "h"):
                    return self.all_in_set(id[1:-1], digits )
            elif ( ( id[0] in ['m', 'b', 'r', '2', 'd' ]) ):
                return self.all_in_set(id[1:], digits )
        elif (len(id) == 8 ):
            if (id[0] == "7"):
                return self.all_in_set(id[1:], digits )
        return True

    def get_courses(self, sc = None):
        if sc:
            courses = self.db.execute_sql( f"""
                SELECT title
                FROM "courses"
                WHERE (title = ?)
            """, (sc,))
        else:
            courses = self.db.execute_sql( f"""
                SELECT title
                FROM "courses"
                """ )
        return [ c[0].lower().strip() for c in courses ]
    
    def collect_course_work_discord_id( self, discord_id ):
        return self.collect_course_work( "discord_id", discord_id )

    def collect_course_work_student_id( self, student_id ):
        return self.collect_course_work( "id", student_id )

    def collect_course_work( self, s_key, s_value ):
        
        courses = self.get_courses()
        course_work = {}

        for c in courses:
            course_list = []
            
            columns = self.db.execute_sql( f"""
                PRAGMA table_info("{c}");
            """)
            logger.debug( f'collect_course_work: columns {columns}')

            me = self.db.execute_sql( f"""
                SELECT *
                FROM "{c}"
                WHERE {s_key} = ?
            """, (s_value,) )

            if (me) and (len(me) > 0):
                me = me[0]

            logger.debug( f'me: {me}')

            if me:
                COURSE_WORK_COLUMN_START = 4
                for ind in range(COURSE_WORK_COLUMN_START, len(me)):
                    if me[ind] != NTNU.SUBMITTED_MARKED:
                        course_list.append( ( columns[ind][1], me[ind] ) )

            if course_list:
                course_work[c] = course_list
        return course_work

    def find_assignment(self, aid, assignments ):
        for course in assignments:
            for wid, wstatus in assignments[course]:
                if ( wid == aid ):
                    return (course, wstatus)
        return None

    def find_student(self, discord_name, course, aid = None):
        aid_s = ""
        if aid:
            aid_s = f", {aid}"
        
        stud = self.db.execute_sql( f"""
SELECT id, name, degree, discord_id {aid_s}
FROM "{course}"
WHERE discord_id LIKE ?
""", ( discord_name, ) )
        return stud

    def find_student_record_discord_id( self, course, discord_id, columns=["*"] ):
        return self.find_student_record( course, "discord_id", discord_id, columns )

    def find_student_record_student_id( self, course, student_id, columns=["*"] ):
        return self.find_student_record( course, "id", student_id, columns )

    def find_student_record( self, course, key, value, columns=None ):
        if columns is not None and len(columns) > 0:
            c_str = ",".join(['"'+ c + '"' for c in columns])
        else:
            c_str = "*"
        stud = self.db.execute_sql( f"""
SELECT {c_str}
FROM "{course}"
WHERE {key} = ?
""", ( value, ) )
        return stud
        
    