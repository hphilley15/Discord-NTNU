import argparse
import jbteach_bot as ubot
import json
import logging
import sys
import os


logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler() )

bot = None

def main( argv = None ):
    global bot
    if argv is None:
        argv = sys.argv[1:]
    parser = argparse.ArgumentParser()

    parser.add_argument('--token', help="Discord API token. Or environment variable TOKEN")
    parser.add_argument('--database', default="Database/ntnu_teaching_2020_2.sqlite", help="Sqlite3 database")
    parser.add_argument('--init', action="store_true", help="Initialize database")
    parser.add_argument('--university', help='Name of university' )

    args = parser.parse_args( argv )

    if not args.token:
        args.token = os.environ["TOKEN"]

    bot = ubot.UniBot()

    bot.start_bot( args.database, args.token, args.university ) 
    
if __name__ == "__main__":
    main( )

