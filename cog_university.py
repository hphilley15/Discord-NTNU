import discord
from discord.ext import commands
from discord.utils import get
import logging

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class University( commands.Cog ):
    def __init__(self, ubot, db ):
        self.ubot = ubot
        self.bot = self.ubot.bot
        self.db = db

    @commands.Cog.listener()
    async def on_message( self, message):
        
        if ( message.channel.type == discord.ChannelType.private or message.channel.type == discord.ChannelType.group ) and message.author != self.bot.user:
            if self.ubot.bot_channel:
                logger.debug( f'bot channel {self.ubot.bot_channel}' )
                user = message.author.name
                await self.ubot.bot_channel.send( content= f"@{message.author.name}#{message.author.discriminator} ({message.author.id}):" + message.content  )
            else:
                logger.info( f'Missing bot channel')
