import discord
from discord.ext import commands
from discord.utils import get
import pathlib
import logging
import random

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class Examination( commands.Cog ):

    def __init__(self, ubot, db, uni ):
        self.ubot = ubot
        self.bot = self.ubot.bot
        self.db = db
        self.uni = uni

    @commands.command(  )
    async def submit( self, ctx, *args ):
        await self.submit_cmd( ctx, args )

    async def submit_cmd( self, ctx, args ):
        try:
            logger.debug( f"Submitting assignment {args}")
            
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator

            dm = author.dm_channel

            message = ctx.message
            if ( message.channel.type != discord.ChannelType.private and message.channel.type != discord.ChannelType.group ):

                await message.delete()

                msg = f"""
                Command submission is only allowed via direct message to @Online Lecturing Support.
                Message removed.
                """
                await self.ubot.send( author, msg )            
            else:
                if ( args is not None ) and ( len(args) > 0 ):
                    aid = args[0].lower()
                    args = args[1:]

                    assignments = self.uni.collect_course_work_discord_id( discord_name )
                    
                    logger.debug( f'assignments: {assignments}')

                    fnd = self.uni.find_assignment( aid, assignments )
                    
                    if fnd:
                        course, _ = fnd
                        stud = self.uni.find_student( discord_name, course, aid )
                        
                        if ( stud is not None ) and ( len(stud) > 0 ):
                            logger.debug( f'student(s) found {stud}' )

                            sid, name, degree, r_discord_id, a_status  = stud[0]
                            logger.debug( f'Student Info: {sid} {name} {degree} {r_discord_id} {a_status} {args}' )

                            if a_status is None:
                                a_status = 0
                                
                            if len(args) > 0:
                                logger.debug( f'' )
                                commit = args[0]
                                args = args[1:]

                                if ( a_status > 0 ):
                                    logger.debug( f'committing {commit}')
                                    if commit in [ 'honesty', 'code', 'report' ]:
                                        fn_id = sid.split()[0]
                                        fdir = pathlib.Path( f'Database/Courses/{course}/{aid}/{fn_id}/' )
                                        fdir.mkdir( exist_ok = True, parents=True )
                                        
                                        for atmnt in message.attachments:
                                            fname = fdir / atmnt.filename
                                        
                                            logger.debug( 'Saving file {fname}')
                                            await atmnt.save( fname )
                                            await self.ubot.send( author, f'Successfully submitted {commit} {atmnt.filename}' )    
                                    else:
                                        await self.ubot.send( author, f'Unknown submission type {commit}')
                                elif ( a_status == 0 ) and ( commit == 'honesty' ):
                                    logger.debug( f'submitting honesty declaration' )
                                    if ( len(message.attachments) > 0 ) and ( message.attachments[0].filename == 'honesty.txt' ):
                                        atmnt = message.attachments[0]

                                        fn_id = sid.split()[0]
                                        fdir = pathlib.Path( f'Database/Courses/{course}/{aid}/{fn_id}/' )
                                        fdir.mkdir( exist_ok = True, parents=True )
                                        
                                        fname = fdir / f'{fn_id}_honesty.txt'
                                        
                                        logger.debug( f'Found honesty declaration for {fn_id}' )
                                        
                                        await atmnt.save( fname )

                                        sql = f'''
                                            UPDATE "{course}" 
                                            SET {aid} = 1
                                            WHERE id = "{sid}";
                                        '''
                                        ret = self.db.execute_sql( sql )
                                        logger.debug( f'update honesty state {ret}')
                                        if ret.rowcount == 1:
                                            await self.ubot.send( author, f'Submission of honesty declaration successful')
                                        else:
                                            await self.ubot.send( author, f'Submission of honesty declaration failed {ret.rowcount}')
                                    else:
                                        await self.ubot.send( author, f'You must attach your honesty declaration with honesty.txt', file=discord.File( 'Database/honesty.txt' ) )
                                else:
                                    logger.debug( f'Missing honesty declaration' )
                                    await self.ubot.send( author, f'You must first submit the honesty declaration via !submit {aid} honesty' )
                            else:
                                await self.ubot.send( author, f'Specify type of submission honesty/code/report' )
                        else:
                            await self.ubot.send( author, f'You are not registered in the course {course}. Register with !register <student_id>' )
                    else:
                        await self.ubot.send( author, f'Unable to find course material {aid}. You currently have the following open course work: {assignments}' )    
                else:
                    await self.ubot.send( author, f'Missing assignment id' )
        except:
            await self.ubot.send(author, f"submit command failed")
            raise

    @commands.command(  )
    async def take( self, ctx, *args ):
        await self.take_cmd( ctx, args )

    async def take_cmd( self, ctx, args ):
        logger.debug( f"Requesting examination {args}")
        try:
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator

            dm = author.dm_channel

            message = ctx.message
            if ( message.channel.type != discord.ChannelType.private and message.channel.type != discord.ChannelType.group ):

                await message.delete()

                msg = f"""
                Requesting a midterm is only allowed via direct message to @Online Lecturing Support.
                Message removed.
                """
                await self.ubot.send( author, msg )            
            else:
                if ( args is not None ) and ( len(args) > 0 ):
                    course = args[0].lower()
                    args = args[1:]
                    courses = self.uni.get_courses( )

                    if course in courses:
                        logger.debug( f"Found course {course}")
                        stud = self.uni.find_student_record_discord_id( course, discord_name )
                        if stud:
                            stud = stud[0]
                            logger.debug( f"Found student {stud}")
                            id = stud[0]
                            if ( args is not None ) and ( len(args) > 0 ):
                                aid = args[0].lower()
                                args = args[1:]
                                fdir = pathlib.Path( f'Database/Courses/{course}/{aid}/exams/' )
                                if fdir.is_dir():
                                    logger.debug( f"Found course directory {str(fdir)}")
                                    locked = False
                                    exams = []

                                    for f in fdir.iterdir():
                                        logger.debug( f"Checking file {str(f)}")
                                        if f.name == "locked":
                                            locked = True
                                            break
                                    if not locked:
                                        exam = self.db.execute_sql( f"""
                                            SELECT file
                                            FROM "{course}-{aid}"
                                            WHERE id = ?
                                            """, ( id, ) )
                                        in_table = False
                                        if exam is None or len(exam) == 0:
                                            for f in fdir.iterdir():
                                                logger.debug( f"Checking file {str(f)}")
                                                if ( f.name.endswith(".html") ) and "solution" not in f.name:
                                                    exams.append( f )
                                            if ( not locked ):
                                                logger.debug(f"Found the following exams {str(exams)}")
                                                exam = str( random.choice( exams ) )
                                                logger.debug(f"Selected exam {exam}")
                                        else:
                                            logger.debug( f'Found exam {exam} in table')
                                            exam = exam[0][0]
                                            in_table = True
                                        if exam:
                                            await self.ubot.send( author, f"Student {id}/{discord_name}, examination {aid} for course {course}. Good luck! {exam}", file=discord.File( exam )  )
                                            if not in_table:
                                                ret = self.db.execute_sql( f"""
                                                    INSERT OR REPLACE INTO "{course}-{aid}" (id, file)
                                                    VALUES (?, ?)
                                                """, ( id, exam ) )

                                        else:
                                            await self.ubot.send( author, f"Cannot find exam")
                                    else:
                                        await self.ubot.send( author, f"Examination {aid} for course {course} is not available for download at this moment")    
                                else:
                                    await self.ubot.send( author, f"Directory {str(fdir)} does not exist. Exam not open.")    
                            else:
                                await self.ubot.send( author, f"Missing exam type (final/midterm) for course {course}")
                        else:
                            await self.ubot.send( author, f"Your id {discord_name} is not registered in the course {course}")
                    else:
                        await self.ubot.send( f"Invalid course name. {course} not in {courses}")
                else:
                    await self.ubot.send( author, "Missing course name")
        except:
            await self.ubot.send(author, "take_cmd failed")
            raise