# -*- utf-8 -*-

import aiohttp
import asyncio
import logging
from bs4 import BeautifulSoup

logger=logging.getLogger( __name__ )
logger.setLevel( logging.DEBUG )

async def scrape( url, limit = 1 ):
    text = ""
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text = await response.text()
    #print('text', text)
    soup = BeautifulSoup(text.encode("utf-8") , 'html.parser')

    text = ""
    count = 1
    for t in soup.find_all( isAnnouncement ):
        text = text + f"{count}.) " + t.text + "\n"
        count = count + 1
        if count > limit:
            break

    print( text )
    return text

def isAnnouncement( tag ):
    ret = False
    if tag.name == 'li':
        #print("Found li")
        if tag.parent.name == 'ol' or tag.parent.name == 'ul':
            #print("Found parent" ) #, tag.parent)
            prev = tag.parent.previous_sibling
            if prev and prev.name[0] == 'h' and prev.name[1] >= '0' and prev.name[1] <= '9':
                #print("Found prev header")
                if 'Announcement' in prev.text or 'Notice' in prev.text or 'Update' in prev.text:
                    print("Found announcements")
                    ret = True
    return ret

def main( argv = None ):
    loop = asyncio.get_event_loop()
    loop.run_until_complete( scrape( url='https://docs.google.com/document/d/e/2PACX-1vTn8fpXPt4GqZwAlETH10s2E0-znJlbI__V8vlI9tytseezo6oIWbxlX6c1n2v-GkzQTtk222296BMK/pub', limit=2 ) )   
    loop.close()

if __name__ == "__main__":
    main( )
