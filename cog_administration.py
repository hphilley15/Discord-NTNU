import discord
from discord.ext import commands
from discord.utils import get

import re
import logging
import random
import asyncio
import pathlib

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class Administration(commands.Cog):
    def __init__(self, ubot, db, uni ):
        self.ubot = ubot
        self.bot = self.ubot.bot
        self.db = db
        self.uni = uni

    @staticmethod  
    def admin_only(self, func):
        def wrapper_admin_only( self, ctx, *args, **kwargs ):
            channel = ctx.channel
            user = self.bot.user
            roles = [ str(r).lower() for r in ctx.author.roles ]
            if "admin" in roles:
                ret = func( self, ctx, *args, **kwargs )
            else:
                ret = None
            return ret
        return wrapper_admin_only

    @commands.command()
    async def admin( self, ctx, *args ):
        "Various administration tasks"
        channel = ctx.channel
        user = self.bot.user
        roles = [ str(r).lower() for r in ctx.author.roles ]
        if "admin" in roles:
            if args and len(args) > 0:
                cmd = args[0]
                if cmd == "add" and len(args) > 2:
                    table = args[1]
                    #self.add_entry( table, args[2:] )
                elif cmd == "search":
                    await self.add_links( ctx )
                elif cmd == "download":
                    await self.add_downloads( ctx, args[1:])
        else:
            await self.ubot.send( ctx, content=f"You are a bad boy {ctx.author}. Only admin users can start a rolecall")

    async def add_links( self, ctx ):
        pass
        #messages = await ctx.channel.history(limit=5000).flatten()
        #num_messages = len(messages)
        #logger.debug( f'searching {num_messages} messages')

    #examRE = re.compile(r'^(?P<name>[^:]+):(?P<id>[^:]+):question_[0-9]+_answer_box_container$')
    examRE = re.compile(r'^(?P<name>[^:]+):(?P<id>[^:]+):question_.*$')

    async def add_downloads(self, ctx, args ):
        try:
            logger.debug( f"cmd download {args}" )
            channel_s = str(ctx.channel).lower()
            if channel_s.endswith("-examinations"):
                course = channel_s[:- len("-examinations")]
                channel = ctx.channel
            else:
                course = channel_s.lower()
                channel_s = channel_s.lower() + "-examinations"
                channel = discord.utils.get(ctx.guild.channels, name=channel_s)
            user = self.bot.user
            if args and len(args) > 0:
                typ = args[0]
                if len(args) > 1:
                    ddir = args[1]
                else:
                    ddir = "."

                if typ == 'exam':
                    droot = pathlib.Path( f'Database/Courses/{course}/{ddir}' )
                    droot.mkdir( exist_ok = True, parents=True )
                    
                    logger.debug( f"cmd download exam from channel {channel_s}" )
                    submissions = 0
                    ignored = 0
                    mcount = 0
                    async for message in channel.history( limit=200):
                        logger.debug( f'searching message {message.content}' )
                        mtext = str(message.content)

                        if ( mcount > 0 ) and ( mtext.startswith( "!admin download exam" ) ):
                            break
                        match = re.match(Administration.examRE, mtext )
                        if match:
                            name = match.group('name')
                            id = match.group('id').upper()

                            logger.debug( f"Found submission: {name} {id}")
                            fname = droot/ f"{name}_{id}_{ddir}.html"
                            if not fname.exists():
                                if len(message.attachments) == 1:
                                    await message.attachments[0].save( fname )
                                    submissions = submissions + 1
                            else:
                                logger.debug(f"Already found a submission for {name} {id}")
                                ignored = ignored + 1
                        mcount += 1
                        
                    await self.ubot.send( ctx.channel, content = f"Found {submissions} submissions, ignored {ignored}" )
        except:
            await self.ubot.send( "add_downloads command failed")
            raise 

    @commands.command()
    async def grades(self, ctx, course: str = None ):
        "Send a list suitable for submitting to the grading system."
        channel = ctx.channel
        user = self.bot.user
        roles = [ str(r) for r in ctx.author.roles ]
        if "admin" in roles:
            await self.grades_cmd( ctx, course )
        else:
            await self.ubot.send(ctx, content=f"You are a bad boy/girl {ctx.author}. Only admin users can start a rolecall")

    async def grades_cmd( self, ctx, course: str = None ):
        try:
            logger.debug("Grades")
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator
            dm = author.dm_channel

            course_list = self.uni.get_courses()
            logger.debug( f'course_list {course_list}' ) 
            msg = "I found the following courses:\n"

            if ( ( course is not None ) and (course != "") and ( course in course_list ) ):
                course_list = [ course ]

            for course in course_list:
                #ct = self.ubot.channelToTable( course )
                logger.debug( f'Checking course {course}')
                msg = course + "\n"

                totals = self.db.execute_sql( f"""
                        SELECT Id, total
                        FROM "{course}-total"
                        ORDER BY Id ASC
                    """ )

                for id, total in totals:
                    letter = self.db.execute_sql( f"""
                            SELECT grade
                            FROM "{course}-grade"
                            WHERE (? >= glimit )
                            ORDER BY glimit DESC
                        """, (total,) )[0][0]
                    msg += f'"{id}", "{letter}"\n'
                await self.ubot.send(author, msg )
        except:
            await self.ubot.send( author, "grades command failed")
            raise

    @commands.command()
    async def rolecall( self, ctx, timeS:str = "10", countS:str = "1" ):
        "Schedule a rolecall in {time seconds} on average for {count times}."
        channel = ctx.channel
        user = self.bot.user
        roles = [ str(r) for r in ctx.author.roles ]
        if "admin" in roles:
            try:
                count = int(countS)
            except ValueError:
                count = 1

            for i in range( count ):
                seed = random.randint(1000,9999)
                await self.ubot.send( ctx, content = f"Rolecall: Send a direct message to {user.mention} with secret number {seed}. You have five minutes.", tts=True )
                try:
                    time = int( timeS )
                except ValueError:
                    time = 3600

                t = time - 300 + random.random() * 600
                if t < 10*60:
                    t = 10*60 + random.random() * 600
                await asyncio.sleep( t )
        else:
            await self.ubot.send(ctx, content=f"You are a bad boy/girl {ctx.author}. Only admin users can start a rolecall")

    @commands.command( hidden=True )
    async def enroll( self, ctx, course, name, id, email ):
        "Enroll a student in a class and adjust the roles"
        logger.debug('enroll', ctx.channel )

        # author = ctx.message.author
        # dm = author.dm_channel

        # logger.debug("")
        # logger.debug( f'dm {dm}' )

        # if not dm:
        #     await author.create_dm()
        
        # logger.debug( "Enrolling", course, name, id, email )
        
        # stRecord = db.get( "students", tdb.Query().studentId == id )
        # if not stRecord:
        #     ret = db.set( "students", { "name": name, "studentId": id, "email": email, "courses": [ course ], "discordId": author.id } )
        # else:
        #     ret = db.addCourseToStudent( id, course )
        
        # if ret:
        #     role = self.ubot.titleToRole( course )
        #     nr = discord.utils.get(author.guild.roles, name=role)
        #     if nr:
        #         try:
        #             await author.add_roles( nr ) #add the role
        #         except Exception as e:
        #             await self.ubot.send(ctx, 'There was an error running this command ' + str(e)) #if error
        #         else:
        #             await self.ubot.send(author, content=f'Enrollment successful' )
        #     else:
        #         await self.ubot.send(ctx, f"Unable to find role {role}" )
        # else:
        #     await self.ubot.send(author, content=f'Unable to enroll in course {course}' )

