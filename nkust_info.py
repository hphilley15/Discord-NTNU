import logging

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class NKUST:

    def __init__(self, db):
        self.db = db

    @staticmethod
    def all_in_set( id, d_set = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ]
 ):
        
        start = 0
        stop = len(id) -1
        
        id = id.lower()
        for i in range(start,stop+1):
            if id[i] not in d_set:
                return False
        return True

    def validateId( self, ids ):
        id = ids.lower()
        
        digits = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ]

        if ( len(id) == 10 ):
            if ( id[0] in ['i', 'f' ]):
                return self.all_in_set( id[1:], digits )
            else:
                return False
        elif (len(id) == 9 ) or ( len(id) == 7):
            return all_in_set( id, digits )
        return False
