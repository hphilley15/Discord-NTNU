import tinydb as tdb
import logging

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class Storage:
    def __init__(self, dbName ):
        self.db = tdb.TinyDB( dbName )

    def get(self, table, query ):
        table = self.db.table( table )
        r = table.get( query )
        return r

    def set( self, table, record ):
        table = self.db.table( table )
        return table.insert( record )

    def getCourses( self, query ):
        return self.get( "courses", query )

    def addCourse( self, course ):
        table = self.db.table( "courses" )

        q = tdb.Query( )
        r = table.get( q.title == course["title"] )

        if "cid" not in course:
            course["cid"] = self.canonicalTitle( course["title"] )

        if ( not r ) or ( len(r) == 0 ):
            logger.debug( f"adding course {course}" )
            table.insert( course )
        else:
            table.upsert( course, q.title == course["title"] )

    def addStudentToDB( self, student ):
        ncourses = []
        for c in student["courses"]:
            cs = self.getCourses( tdb.Query().cid == self.canonicalTitle( c ) )
            if cs:
                ncourses.append( cs['cid'] )
            else:
                logger.warning("Unable to find course", c )
        student['courses'] = ncourses

        table = self.db.table("students")
        q = tdb.Query()
        r = table.get( q.id == student["id"] )
        if ( not r ) or ( len(r) == 0 ):
            logger.debug( f"Adding student {student}" )
            table.insert( student )
        else:
            table.upsert( student )

    def addCourseToStudent( self, sid, course ):
        ret = False
        table = self.db.table("students")
        q = tdb.Query()
        r = table.get( q.id == sid )
        if r:
            title = self.canonicalTitle( course )
            cs = self.getCourses( tdb.Query().cid == self.canonicalTitle( title ) )
            if cs:
                if title not in r["courses"]:
                    r["courses"].append(title)
                    table.update( r )
                    ret = True
        return ret


    def canonicalTitle( self, title ):
        return title.lower().replace(" ","-")

    def setupDB( self, reset = False ):
        if reset:
            self.db.truncate()
            self.db.drop_tables()
 
        self.addCourse( { "title": "Robot Vision", "urls": [ {'text': "Robot Vision Homepage", 'url': "https://docs.google.com/document/d/e/2PACX-1vTmedpN0Wp90ND0HVlvV480Ar3QJN-6NKX8VLbiFLWFC3BM6-9RtqrzwMyqsudROUCKmlaOyRmPWLVv/pub" } ] } )
        self.addCourse( { "title": "Intelligent Humanoid Robotics", "urls": [ { 'text': 'Intelligent Humanoid Robotics Homepage', 'url': "https://docs.google.com/document/d/1w-Vh5SW5bPfwZe23YduV4SWMiSdNMOkGitSwACecOJI/pub" } ] } )
        self.addCourse( { "title": "all", "urls": [ { 'text': "Jacky Baltes Homepage", 'url': "https://www.sites.google.com/site/jackybaltes/home" } ] } )
        self.addCourse( { 'title': 'Test Course', 'urls': [ "link1", "link2" ] } )

        students = self.db.table("students")



    
