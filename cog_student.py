import discord
from discord.ext import commands
from discord.utils import get

import logging

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class Student( commands.Cog ):

    def __init__(self, ubot, db, uni ):
        self.ubot = ubot
        self.bot = self.ubot.bot
        self.db = db
        self.uni = uni

#    @commands.Cog.listener()
#    async def on_message( self, message):
#        channel = None
#        author = message.author
#        logger.debug( f"Rx {message}" )
#        logger.debug( f"Rx Channel {message.channel}" )
#        channel = None
#        if ( message.channel.type == discord.ChannelType.private or message.channel.type == discord.ChannelType.group ) and message.author != self.bot.user:
#            for ch in self.bot.get_all_channels():
#                if ch.name == BOT_CHANNEL:
#                    channel = ch
#                    break
#            user = author.name
#            if channel:
#                await self.ubot.send(channel, content= f"@{user}:" + message.content  )
#        else:
#            await self.bot.process_commands(message)    
    #     if ( ( author.name == "FIRA Scavenger Hunt Webhook" ) ):
    #         logger.info("Received submission from webhook")
    #         data = message.content.splitlines()
    #         logger.debug( f"data {data}" )
    #         sub = dict( map( lambda s: map( str.strip, s.split(':') ), data ) )
    #         sub[ 'answer' ] = int( sub['answer'] )
    #         sub[ 'seed' ] = int( sub['seed'] )
            
    #         logger.debug( f'Submission {sub}' )
    #         await self.submission( sub )

    @commands.command(  )
    async def register( self, ctx, student_id : str, course : str ):
        await self.register_cmd( ctx, student_id, course )

    async def register_cmd( self, ctx, student_id: str, sel_course : str ):
        try:
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator

            dm = author.dm_channel

            if (student_id[0] == "<"):
                student_id = student_id[1:]
            if (student_id[-1] == ">"):
                student_id = student_id[0:-1]

            if ( self.uni.validateId(student_id) ):
                courses = self.uni.get_courses( sel_course )

                logger.debug( f'register course {courses}' )

                if ( ( courses is not None ) and ( len(courses) > 0 ) ):
                    course = courses[0] #self.ubot.channelToTable( courses[0] )
                    logger.debug( f'course exists {course}' )

                    stud = self.db.execute_sql( f"""
                        SELECT id,name, degree, discord_id
                        FROM "{course}"
                        WHERE lower(id) LIKE ?
                    """, ( student_id+'%',) )
                    
                    if ( ( stud is not None ) and ( len( stud ) > 0 ) ):
                        logger.debug( f'student found {stud[0]}')

                        sid, name, degree, r_discord_id = stud[0]
                        logger.debug( f"Student record retrieved {sid},{name}, {degree}, {r_discord_id}" )
                        if r_discord_id is None:
                            sql = f'''
                                REPLACE INTO {course} ( id, name, degree, discord_id )
                                VALUES(?,?,?,?)
                            '''
                            ret = self.db.execute_sql( sql, ( sid, name, degree, discord_name ) )
                            role = discord.utils.get(author.guild.roles, name=self.ubot.course_to_role( course ) )
                            logger.debug( f"Server role {role}" )

                            await author.add_roles( role, reason = f"Registered for course {course}" )
                            await self.ubot.send(author, f"{name}/{student_id} with {discord_name} registered successfully in course {course}")
                        elif r_discord_id == discord_name:
                            await self.ubot.send(author, f"Student {name}/{student_id} with {discord_name} already registered sucessfully" )
                        else:
                            await self.ubot.send(author, f"{name}/{student_id} already registered with discord id {r_discord_id}" )
                    else:
                        await self.ubot.send( author, f"Unable to find students in course {course}")
                else:
                    await self.ubot.send(author, f"Invalid course {sel_course}" )
            else:
                await self.ubot.send(author, f"Invalid student id {student_id}" )
            
                # exists = self.db.execute_sql(f'''
                #     SELECT 
                #         id
                #     FROM 
                #         registration 
                #     WHERE 
                #         ( course=? AND (discord_id=? OR id=?)
                # ''', ( course, student_id, discord_id ) )
                # logger.debug('exists', exists )
                # if ( exists is not None ) and ( len(exists) > 0 ):
                #     await self.ubot.send(author, f'User {name} with id {student_id} and discord_id {discord_id} already registered for course {course}')
                # else:
                #     ret = self.db.execute_sql( f'''
                #         INSERT INTO registration( student_id, discord_id, course )
                #         VALUES
                #             (?,?,?)
                #     ''', ( student_id, discord_id, course ) )
                #     logger.debug( f"insert ret {ret}" )
                #     if ret is not None:
                #         await self.ubot.send(author, f'User {name} discord_id {discord_id} registered successfully')
        except:
            await self.ubot.send( author, "register command failed")
            raise 

    @commands.command(  )
    async def marks( self, ctx ):
        await self.marks_cmd( ctx )

    async def marks_cmd( self, ctx ):
        try:
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator

            tables = [t[0] for t in self.db.execute_sql( f"""
                SELECT name 
                FROM sqlite_master 
                WHERE type='table'
            """) ]

            logger.debug( f'found tables {tables}' )

            courses = self.uni.get_courses()

            sent = False
            for c in courses:
                #ct = self.ubot.channelToTable( c )
                logger.debug( f'checking course {c}')

                msg = f'Course: {c}' + '\n'
                mid = self.db.execute_sql( f"""
                    SELECT id
                    FROM "{c}"
                    WHERE (discord_id = ?)
                """, (discord_name,) )

                if mid:
                    mid = mid[0][0].upper().strip()
                    logger.debug( f"found id {mid} for discord_id {discord_name} in course {c}")

                    for exam in [ "midterm", "final_exam" ] + [ "assignment_" + str(i) for i in range(1,10) ]:
                        cstr = c + "-" + exam #cstr = self.ubot.channelToTable( c + "-" + exam )
                        logger.debug( f"looking for table {cstr}")
                        if  cstr in tables:
                            logger.debug( f'found exam {cstr}')
                        
                            marks = self.db.execute_sql( f"""
                                SELECT id, mark, comments
                                FROM "{cstr}"
                            """)

                            for tid, tmark, tcomments in marks:
                                tid = tid.upper().strip()
                                logger.debug( f'Check mark table {mid} {tid} {tid in mid} {tmark} {tcomments}')
                                if tid in mid: 
                                    msg = msg + f'{exam}' + ':\n'
                                    if tmark:
                                        msg = msg + f'Marks {int(float(tmark)*100)}%\n'
                                    else:
                                        msg = msg + f'No mark recorded\n'
                                    if tcomments is None:
                                        tcomments = 'No comments'
                                    msg = msg + f'Comments' + '\n' + tcomments + '\n'
                    
                    
                    # r_total = self.db.execute_sql( f"""
                    #     SELECT total
                    #     FROM "{c}-total"
                    #     WHERE (Id = ?)
                    # """, (mid,) )

                    # logger.debug( f'r_total {r_total}')

                    # if ( ( r_total is not None ) and ( len(r_total) > 0 ) ):
                    #     total = r_total[0][0]

                    #     r_letter = self.db.execute_sql( f"""
                    #         SELECT grade
                    #         FROM "{c}-grade"
                    #         WHERE (? >= glimit )
                    #         ORDER BY glimit DESC
                    #     """, (total,) )

                    #     logger.debug( f'r_letter {r_letter}' )

                    #     letter = r_letter[0][0]

                    #     msg += f"Total: {total*100:.2f}%\n"
                    #     msg += f"Grade: {letter}\n"
                    # else:
                    #     logger.debug("Unable to retrieve grade")

                    sent = True
                    await self.ubot.send( author, msg )
            if not sent:
                msg = f'Discord id {discord_name} is not registered in any course'
                await self.ubot.send( author, msg )
        except:
            await self.ubot.send( author, "marks command failed")
            raise



    # @commands.command( )
    # async def play( self, ctx ):
    #     await self.play_cmd( ctx )

    # async def play_cmd( self, ctx ):
    #     channel = ctx.channel
    #     author = ctx.message.author
    #     name = author.display_name
    #     discord_id = author.id
    #     dm = author.dm_channel
    #     p = self.db.get_participant( discord_id )
    #     if p:
    #         logger.info( f"Found participant {p}" )
    #         msgs, files, next_state, ann = game.next( discord_id )
    #         for i,m in enumerate(msgs):
    #             dFiles = []
    #             if files and len(files) > i:
    #                 logger.debug( '***' + str(files[i]) )
    #                 if files[i] and len(files[i]) > 0:
    #                     for fobj, fname  in files[i]:
    #                         dFiles.append( discord.File( fp=fobj, filename=fname ) )
    #             if dFiles and len(dFiles) > 0:
    #                 try:
    #                     await self.ubot.send(author, content=m, files=dFiles )
    #                 except Exception as e:
    #                     logger.warning( f"Trying to send the response with files failed {e}" )
    #             else:
    #                 await self.ubot.send(author, content=m )

    #         p['state'] = next_state
    #         self.db.update_participant( p )
    #         if ann:
    #             ch = find_channel( self.bot, "fira-scavenger-hunt" )
    #             if ch:
    #                 await self.ubot.send(ch, ann )    
    #     else:
    #         await self.ubot.send(author, f'User {name} is not registered. Use !register command before you begin playing the game.' )

    # @commands.command( )
    # async def submit( self, ctx, solution : int ):
    #     await self.submit_cmd( ctx, solution )

    # async def submit_cmd( self, ctx, solution: int ):
    #     logger.info( f"Submit command {solution}" )
    #     channel = ctx.channel
    #     author = ctx.message.author
    #     name = ctx.author.display_name
    #     discord_id = author.id
    #     dm = author.dm_channel
    #     p = self.db.get_participant( discord_id )
    #     solved, msg, ann = self.game.answer( discord_id, solution )
    #     await self.ubot.send(dm, msg )
    #     if ann:
    #         ch = find_channel( self.bot, "fira-scavenger-hunt")
    #         if ch:
    #             await self.ubot.send(ch, ann )
        
