from discord.ext import commands
from discord.utils import get

import asyncio
import json
import logging
import discord 
import random
import asyncio
import webscraper as ws

import jb_database as jbd

import cog_university
import cog_student
import cog_information
import cog_examination
import cog_administration


import ntnu_info
import nkust_info

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )


# class JackyId( commands.Cog ):
#     def __init__(self, bot ):
#         self.bot = bot

#     @commands.Cog.listener()
#     async def on_message( self, message):
#         logger.debug( f'Guild {bot.private_channels} checking message {message}' )
#         if ( isinstance(message.channel, discord.channel.DMChannel) ):
#             dmc = message.channel
#             recvr = dmc.recipient
#             roles = [ r.name.lower() for r in recvr.roles ]
#             if ( "jacky_id" in roles ):
#                 channel = discord.utils.get( bot.guild.get_all_channels(), name="jacky_id" )
#                 if channel:
#                     await channel.send( content= f"@{message.author.id}:" + message.content  )
#         await self.bot.process_commands(message)

   
class UniBot:
    BOT_CHANNEL = "bot-dm"
    def __init__(self ):
        self.bot = commands.Bot( command_prefix='!')
        self.bot.add_listener( self.make_on_ready() )
        self.bot_channel = None
        self.db = None
        self.uni = None 

    def make_on_ready( self ):
        async def on_ready():
            logger.debug( f'On_ready')
            self.bot_channel = self.find_channel( self.BOT_CHANNEL )
            logger.debug( f'Setting up bot channel {self.bot_channel}')
        return on_ready

    def run(self, *args, **kwargs ):
        self.bot.run( * args, **kwargs )

    def add_cog( self, *args, **kwargs ):
        self.bot.add_cog( * args, **kwargs )

    async def send(self, sctxt, *args, **kwargs ):
        await sctxt.send( *args, **kwargs )
        await self.bot_channel.send( f'Replying {sctxt}, {args}, {kwargs}' )

    def find_channel( self, cname ):
        channel = None
        for ch in self.bot.get_all_channels():
            if ch.name == cname:
                channel = ch
                break
        #channel = discord.utils.get( self.bot.channels, cname )
        return channel

    @staticmethod
    def findURL( lst, url ):
        ind = -1
        for i,l in enumerate( lst ):
            if l['url'] == url:
                ind = i
                break
        return ind

    @staticmethod
    def titleToRole( title ):
        return title.lower().replace(" ", "-" ).replace("#","-")

    # @staticmethod
    # def channelToTable( channel ):
    #     if channel[0] == "'" or channel[0] == '"':
    #         channel = channel[1:]
    #     if channel[-1] == "'" or channel[-1] == '"':
    #         channel = channel[:-1]
        
    #     return channel.lower().replace("#","-") #.replace("-", "_" )

    @staticmethod
    def course_to_role( course ):
        if course[0] == "'" or course[0] == '"':
            course = course[1:]
        if course[-1] == "'" or course[-1] == '"':
            course = course[:-1]
        return course

    def start_bot(self, db_name, token, uni):
        self.db = jbd.JB_Database( db_name )

        if uni.lower() == 'ntnu':
            self.uni = ntnu_info.NTNU( self.db )
        elif uni.lower() == 'nkust':
            self.uni = nkust_info.NKUST( self.db )

        logger.debug(f'start_bot uni {uni} => {self.uni}')

        self.add_cog( cog_university.University( self, self.db ) )
        #bot.add_cog( JackyId(bot) )
        self.add_cog( cog_student.Student(self, self.db, self.uni ) )
        self.add_cog( cog_information.Information( self, self.db, self.uni ) )
        self.add_cog( cog_administration.Administration( self, self.db, self.uni ) )
        self.add_cog( cog_examination.Examination( self, self.db, self.uni ) )

        self.run(token)  # Where 'TOKEN' is your bot token
        
