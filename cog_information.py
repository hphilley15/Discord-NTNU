import discord
from discord.ext import commands
from discord.utils import get
import logging
import datetime

logger = logging.getLogger(__name__)
logger.setLevel( logging.DEBUG )

class Information( commands.Cog ):
    def __init__(self, ubot, db, uni ):
        self.ubot = ubot
        self.bot = self.ubot.bot
        self.db = db
        self.uni = uni

    @commands.Cog.listener()
    async def on_member_join( self, member ):
        await self.ubot.send( member, content='''
        Welcome to the teaching support bot. You can type "!help" for a list of commands.
        Send a direct message to the online lecturing support bot
        with !register <student_id> <course>. Note that you must have
        pre-registered with your <student_id>, that is, I must have your <student_id>
        on the official class list, to be able to register for the course.
        ''' )


    @commands.command( )
    async def courses(self, ctx ):
        "Sends a list of currently active courses. If you are registered in a course, then you should have access to the text and voice channel with the same name."
        await self.courses_cmd( ctx )
    
    async def courses_cmd(self, ctx ):
        try:
            logger.debug("Courses")
            channel = ctx.channel
            user = self.bot.user
            author = ctx.message.author
            name = author.display_name
            discord_id = author.id
            discord_name = author.name + '#' + author.discriminator
            dm = author.dm_channel

            course_list = self.uni.get_courses()

            logger.debug( f'course_list {course_list}' ) 
            msg = "I found the following courses:\n"

            for course in course_list:
                #ct = self.ubot.channelToTable( course )
                logger.debug( f'Checking course {course}')
                
                reg = self.db.execute_sql( f"""
                    SELECT id 
                    FROM "{course}"
                    WHERE id=?
                """, ( discord_name, ) )
                
                if ( (reg is not None ) and ( len(reg) > 0 ) ):
                    msg = msg + course + "*" + '\n'
                else:
                    msg = msg + course + '\n'
                
            await self.ubot.send(author, msg)
        except:
            await self.ubot.send(author, f"courses command failed")
            raise

    @commands.command( )
    async def links(self, ctx):
        await self.links_cmd( ctx )

    async def links_cmd( self, ctx ):
        "Sends a message with important links for the user to the user's direct messaging channel"
        try:
            logger.debug( f'links {ctx.channel}')

            author = ctx.message.author
            dm = author.dm_channel
            channel_name = ctx.channel.name

            slides = []
            assignments = []

            async for m in ctx.channel.history( limit=5000):
                #logger.debug( f'searching message {m.content}' )
                #TODO: Figure out how to set cut-off date
                if m.created_at < datetime.datetime(2021, 2, 1 ):
                    break

                mtext = str(m.content).strip()
                if mtext[0:5].lower() == "slide":
                    logger.debug( f'found slide')
                    slides.append(m.content)
                elif mtext[0:6].lower() == "assign":
                    logger.debug( f'found slide')
                    assignments.append(m.content)
                # else:
                #     logger.debug( f'found message with no link in {mtext}')
            if slides:
                await self.ubot.send( author, "\n".join(slides) )

            if assignments:
                await self.ubot.send( author, "\n".join(assignments) )
        except:
            await self.ubot.send( "links command failed")
            raise
